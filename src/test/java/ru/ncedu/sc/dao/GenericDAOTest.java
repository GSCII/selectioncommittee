package ru.ncedu.sc.dao;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.io.Serializable;

import static org.junit.Assert.*;


/**
 * Created by Gorbatovskiy on 19.02.2016.
 */
public class GenericDAOTest {
    private GenericDAO<TestEntity, Integer> testDAO;
    private TestEntity entity;

    @Before
    public void setUp() {
        testDAO = new GenericDAO<TestEntity, Integer>(TestEntity.class) {
        };
        entity = new TestEntity();
        entity.setName("TEST");
    }

    @Entity
    @Table(name = "TEST")
    private static class TestEntity {
        @Id
        @GeneratedValue(generator = "increment")
        @GenericGenerator(name = "increment", strategy = "increment")
        @Column(name = "ID")
        private Integer id;

        @Column(name = "NAME")
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            TestEntity entity = (TestEntity) o;

            if (!id.equals(entity.id)) return false;
            return name.equals(entity.name);

        }

        @Override
        public int hashCode() {
            int result = id.hashCode();
            result = 31 * result + name.hashCode();
            return result;
        }
    }

    @Ignore
    @Test
    public void testCreateAndDelete() throws Exception {
        Integer primaryKey = (Integer) testDAO.create(entity);
        assertNotNull("Entry was not created", primaryKey);
        entity.setId(primaryKey);
        testDAO.delete(entity);
    }

    @Ignore
    @Test
    public void testRead() throws Exception {
        entity.setId((Integer) testDAO.create(entity));
        try {
            assertSame(testDAO.read(entity.getId()), entity);
        } finally {
            testDAO.delete(entity);
        }
    }

    @Ignore
    @Test
    public void testUpdate() throws Exception {
        String newName = "hello, testing world!";
        entity.setName(newName);
        entity.setId((Integer) testDAO.create(entity));
        try {
            assertNotEquals(testDAO.read(entity.getId()), newName);
        } finally {
            testDAO.delete(entity);
        }
    }

    @Ignore
    @Test
    public void testReadAll() throws Exception {
        assertTrue(testDAO.readAll().size() == 1);
    }

    @Ignore
    @Test
    public void testFindByQuery() throws Exception {
        assertTrue(testDAO.findByQuery("SELECT * FROM TEST WHERE NAME = 'new name'").size() == 1);
    }

    @Ignore
    @Test
    public void testDelete() throws Exception {
        entity = testDAO.read(1);
        testDAO.delete(entity);
        assertNull(testDAO.read(1));
    }
}