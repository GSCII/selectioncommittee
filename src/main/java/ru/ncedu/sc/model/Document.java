package ru.ncedu.sc.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by gorbatovskiy on 09.02.16.
 */
@Entity
@Table(name = "SC_DOCUMENT")
public class Document {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "ID")
    private Integer id;

    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "DOC_NUMBER")
    private String number;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "TYPE_ID")
    private DocumentType type;

    @Column(name = "ISSUED_ON")
    private Date issuedOn;

    @Size(max = 100)
    @Column(name = "AUTHORITY")
    private String authority;

    @Column(name = "AUTHENTICITY")
    private boolean authenticity;

    public Document() {
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public DocumentType getType() {
        return type;
    }

    public void setType(DocumentType type) {
        this.type = type;
    }

    public Date getIssuedOn() {
        return issuedOn;
    }

    public void setIssuedOn(Date issuedOn) {
        this.issuedOn = issuedOn;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public boolean isAuthenticity() {
        return authenticity;
    }

    public void setAuthenticity(boolean authenticity) {
        this.authenticity = authenticity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
