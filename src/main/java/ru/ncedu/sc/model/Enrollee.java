package ru.ncedu.sc.model;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by gorbatovskiy on 09.02.16.
 */
@Entity
@Table(name = "SC_ENROLLEE")
public class Enrollee {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "ID")
    private Integer id;

    @NotNull
    @Size(min = 4, max = 25)
    @Column(name = "FIRST_NAME")
    private String firstName;

    @NotNull
    @Size(min = 4, max = 25)
    @Column(name = "MIDDLE_NAME")
    private String middleName;

    @NotNull
    @Size(min = 4, max = 25)
    @Column(name = "LAST_NAME")
    private String lastName;

    @NotNull(message = "Дата должна быть задана")
    @Column(name = "BIRTHDATE")
    private Date birthDate;

    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CITIZENSHIP")
    private String citizenship;

    @Size(max = 45)
    @Column(name = "REGISTRATION")
    private String registration;

    @Size(max = 45)
    @Column(name = "DOMICILE")
    private String domicile;

    @Size(max = 11)
    @Column(name = "HOME_PHONE")
    private String homePhone;

    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "MOBILE_NUMBER")
    private String mobileNumber;

    @Size(max = 45)
    @Column(name = "PARENT_NAME")
    private String parentName;

    @Size(max = 11)
    @Column(name = "PARENT_NUMBER")
    private String parentNumber;

    @NotNull
    @Column(name = "BENEFITS")
    private boolean hasBenefits;

    @NotNull
    @Column(name = "MATERIALS")
    private boolean hasMaterials;

    @NotNull
    @Column(name = "HOSTEL")
    private boolean hostelProvision;

    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "SCHOOLNAME")
    private String schoolName;

    @NotNull
    @Max(2200)
    @Min(1900)
    @Column(name = "GRADUATION_YEAR")
    private int graduationYear;

    @NotNull
    @Max(5)
    @Min(1)
    @Column(name = "MARK")
    private float mark;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "FOREIGN_LANG_ID")
    private Language foreignLanguage;

    @Valid
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "PASSPORT_ID")
    private Document passport;

    @Valid
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "DIPLOMA_ID")
    private Document diploma;

    public Enrollee() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getDomicile() {
        return domicile;
    }

    public void setDomicile(String domicile) {
        this.domicile = domicile;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentNumber() {
        return parentNumber;
    }

    public void setParentNumber(String parentNumber) {
        this.parentNumber = parentNumber;
    }

    public boolean isHasBenefits() {
        return hasBenefits;
    }

    public void setHasBenefits(boolean hasBenefits) {
        this.hasBenefits = hasBenefits;
    }

    public boolean isHasMaterials() {
        return hasMaterials;
    }

    public void setHasMaterials(boolean hasMaterials) {
        this.hasMaterials = hasMaterials;
    }

    public boolean isHostelProvision() {
        return hostelProvision;
    }

    public void setHostelProvision(boolean hostelProvision) {
        this.hostelProvision = hostelProvision;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public int getGraduationYear() {
        return graduationYear;
    }

    public void setGraduationYear(int graduationYear) {
        this.graduationYear = graduationYear;
    }

    public float getMark() {
        return mark;
    }

    public void setMark(float mark) {
        this.mark = mark;
    }

    public Language getForeignLanguage() {
        return foreignLanguage;
    }

    public void setForeignLanguage(Language foreignLanguage) {
        this.foreignLanguage = foreignLanguage;
    }

    public Document getPassport() {
        return passport;
    }

    public void setPassport(Document passport) {
        this.passport = passport;
    }

    public Document getDiploma() {
        return diploma;
    }

    public void setDiploma(Document diploma) {
        this.diploma = diploma;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
