package ru.ncedu.sc.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by gorbatovskiy on 10.02.16.
 */
@Entity
@Table(name = "AUTHORITIES")
public class UserRole {

    @Id
    @Column(name = "USERNAME")
    private String login;

    @Column(name = "AUTHORITY")
    private String name;

    public UserRole() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
