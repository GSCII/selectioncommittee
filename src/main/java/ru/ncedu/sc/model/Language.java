package ru.ncedu.sc.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by gorbatovskiy on 09.02.16.
 */
@Entity
@Table(name = "SC_LANGUAGE_REF")
public class Language {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    public Language() {
    }

    @Override
    public String toString() {
        return name;
    }

    public Language(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
