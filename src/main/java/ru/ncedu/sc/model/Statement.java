package ru.ncedu.sc.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by gorbatovskiy on 10.02.16.
 */
@Entity
@Table(name = "SC_STATEMENT")
public class Statement {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "ENROLLEE_ID")
    private Enrollee enrollee;

    @ManyToOne
    @JoinColumn(name = "SPECIALITY_ID")
    private Speciality speciality;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User creator;

    @Column(name = "PRIORITY")
    private int priority;

    @Column(name = "FULL_TIME")
    private boolean fullTime;

    @Column(name = "BUDGET")
    private boolean budget;

    @Column(name = "RELEVANCE")
    private boolean relevance;

    @Column(name = "DATE_CHANGES")
    private Date changesDate;

    public Statement() {
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public Enrollee getEnrollee() {
        return enrollee;
    }

    public void setEnrollee(Enrollee enrollee) {
        this.enrollee = enrollee;
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Date getChangesDate() {
        return changesDate;
    }

    public void setChangesDate(Date changesDate) {
        this.changesDate = changesDate;
    }

    public boolean isFullTime() {
        return fullTime;
    }

    public void setFullTime(boolean fullTime) {
        this.fullTime = fullTime;
    }

    public boolean isBudget() {
        return budget;
    }

    public void setBudget(boolean budget) {
        this.budget = budget;
    }

    public boolean isRelevance() {
        return relevance;
    }

    public void setRelevance(boolean relevance) {
        this.relevance = relevance;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Statement{" +
                "id=" + id +
                ", enrollee=" + enrollee +
                ", creator=" + creator +
                ", speciality=" + speciality +
                ", priority=" + priority +
                ", budget=" + budget +
                ", fullTime=" + fullTime +
                ", relevance=" + relevance +
                ", changesDate=" + changesDate +
                '}';
    }
}
