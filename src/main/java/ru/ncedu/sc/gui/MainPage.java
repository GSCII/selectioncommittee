package ru.ncedu.sc.gui;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import ru.ncedu.sc.gui.enrollee.EnrolleeGrid;
import ru.ncedu.sc.gui.enrollee.EnrolleeView;
import ru.ncedu.sc.gui.enrollee.EnrolleeViewByTable;
import ru.ncedu.sc.gui.view.PropertiesView;
import ru.ncedu.sc.gui.view.SpecialtyView;
import ru.ncedu.sc.gui.view.UsersView;

/**
 * Created by Gorbatovskiy on 20.02.2016.
 */
@Theme("tests-valo-metro")
public class MainPage extends UI {
    private HorizontalLayout root;
    private VerticalLayout menu;
    private VerticalLayout content;
    private Navigator navigator;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        getCurrent().getPage().setTitle("Weeee");

        content = new VerticalLayout();
        content.setSizeFull();

        navigator = new Navigator(this, content);
        navigator.addView(
                MenuItems.ENROLLEE.getUri(),
                EnrolleeView.class);
        navigator.addView(
                MenuItems.SPECIALITY.getUri(),
                SpecialtyView.class);
        navigator.addView(
                MenuItems.USERS.getUri(),
                UsersView.class);
        navigator.addView(
                MenuItems.PROPERTIES.getUri(),
                PropertiesView.class);
        navigator.navigateTo(MenuItems.ENROLLEE.getUri());

//        // <test-area>
//        navigator.addView("test", TestPage.class);
//        navigator.navigateTo("test");
//        // </test-area>

        menu = new MenuLayout(navigator);
        menu.setWidth(200f, Unit.PIXELS);
        menu.setHeightUndefined();
        menu.setPrimaryStyleName("valo-menu");
        menu.setMargin(new MarginInfo(true, false, false, false));

        root = new HorizontalLayout();
        root.setSizeFull();
        root.addComponents(menu, content);
        root.setExpandRatio(content, 1); // Растягиваем только контент
        setContent(root);
    }
}
