package ru.ncedu.sc.gui;

import java.lang.String;

import com.vaadin.server.FontAwesome;

/**
 * Created by Gorbatovskiy on 27.02.2016.
 * Вся информация о кнопках меню в одном месте.
 */
public enum MenuItems {
    ENROLLEE(
            "enrollees",
            "Абитуриент",
            "Панель абитуриентов",
            FontAwesome.MORTAR_BOARD
    ),
    SPECIALITY(
            "specialties",
            "Специальность",
            "Панель специальностей",
            FontAwesome.BOOK
    ),
    USERS(
            "users",
            "Пользователь",
            "Панель пользователей",
            FontAwesome.USERS
    ),
    PROPERTIES(
            "properties",
            "Система",
            "Панель настроек",
            FontAwesome.GEARS
    );

    private String uri;
    private String name;
    private String description;
    private FontAwesome icon;

    MenuItems(String uri, String name, String description, FontAwesome icon) {
        this.uri = uri;
        this.name = name;
        this.description = description;
        this.icon = icon;
    }

    public String getUri() {
        return uri;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public FontAwesome getIcon() {
        return icon;
    }
}
