package ru.ncedu.sc.gui.view;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;

/**
 * Created by Gorbatovskiy on 27.02.2016.
 */
public class SpecialtyView extends VerticalLayout implements View {

    public SpecialtyView() {
        setSizeFull();
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        Notification.show("Speciality");
    }
}
