package ru.ncedu.sc.gui;


import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;

/**
 * Created by Gorbatovskiy on 27.02.2016.
 */
public class MenuLayout extends VerticalLayout {
    private Navigator navigator;

    public MenuLayout(final Navigator navigator) {
        this.navigator = navigator;

        setSizeFull();
        setSpacing(true);

        for (final MenuItems item : MenuItems.values()) {
            Button menuButton = new Button();
            menuButton.setCaption(item.getName());
            menuButton.setIcon(item.getIcon());
            menuButton.setDescription(item.getDescription());
            menuButton.setWidth(100f, Unit.PERCENTAGE);
            menuButton.addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent clickEvent) {
                    navigator.navigateTo(item.getUri());
                }
            });
            addComponent(menuButton);
        }

    }

}
