package ru.ncedu.sc.gui;

import com.vaadin.ui.*;

import java.util.Iterator;

/**
 * Created by Gorbatovskiy on 11.03.2016.
 */
public class ValidationFormLayout extends FormLayout {

    private boolean validationVisible = false;

    public void setValidationVisible(boolean validationVisible) {
        this.validationVisible = validationVisible;
        for (Iterator<Component> iterator = iterator(); iterator.hasNext(); ) {
            Component component = iterator.next();
            if (component instanceof AbstractField) {
                AbstractField abstractField = (AbstractField) component;
                abstractField.setValidationVisible(validationVisible);
            }
        }
    }

    @Override
    public void addComponent(Component component) {
        component.setWidth("100%");
        if (component instanceof AbstractField) {
            ((AbstractField) component).setValidationVisible(validationVisible);
        }
        if (component instanceof AbstractTextField) {
            ((AbstractTextField) component).setNullRepresentation("");
        }
        super.addComponent(component);
    }

    public boolean isValidationVisible() {
        return validationVisible;
    }
}
