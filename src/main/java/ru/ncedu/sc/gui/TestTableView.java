package ru.ncedu.sc.gui;


import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.ActionManager;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.ui.*;

/**
 * Created by Gorbatovskiy on 28.02.2016.
 */
public class TestTableView extends VerticalLayout implements View, ValueChangeListener, Handler {
    private Table content;
    private Action edit;
    private Action show;

    public TestTableView() {
        edit = new Action("Редактировать");
        show = new Action("Заявления");

        content = new Table();
        content.setImmediate(true);
        content.setSelectable(true);
        content.setSortEnabled(false);
        content.setNullSelectionAllowed(false);
        content.setPageLength(30);
        content.setEditable(false);
        content.setSizeFull();


        // Column definition
        content.addContainerProperty("Full name", String.class, null);
        content.addContainerProperty("Mark", Double.class, null);
        content.addContainerProperty("Materials", CheckBox.class, null);
//        content.addContainerProperty("Documents", CheckBox.class, null);

        // Column description
//        content.setColumnExpandRatio("Full name", 1);
//        content.setColumnExpandRatio("Mark", 1);
//        content.setColumnExpandRatio("Full name", 1);
//        content.setColumnExpandRatio("Full name", 1);

        //
        content.addItem(new Object[]{"weee1", 2.2, new CheckBox("", false)}, "weee1");
        content.addItem(new Object[]{"weee2", 2.2, new CheckBox("", true)}, "weee2");

        content.addValueChangeListener(this);
        content.addActionHandler(this);

//        table.addContainerProperty("First Name", String.class, null);


        addComponent(content);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }

    @Override
    public void valueChange(Property.ValueChangeEvent e) {
        Notification.show(e.getProperty().getValue().toString());
    }

    @Override
    public Action[] getActions(Object target, Object sender) {
        return new Action[]{show, edit};
    }

    @Override
    public void handleAction(Action action, Object sender, Object target) {
        Notification notification = new Notification(action.getCaption().toString());
        notification.setDescription(sender.toString() + " " + target.toString());
        notification.show(Page.getCurrent());
    }
}
