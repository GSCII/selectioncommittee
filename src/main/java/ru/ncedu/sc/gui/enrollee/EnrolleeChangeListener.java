package ru.ncedu.sc.gui.enrollee;

import com.vaadin.util.ReflectTools;

import java.lang.reflect.Method;

/**
 * Created by Gorbatovskiy on 20.03.2016.
 */
public interface EnrolleeChangeListener {
    Method ENROLLEE_CHANGE_METHOD = ReflectTools.findMethod(
            EnrolleeChangeListener.class, "enrolleeChange", EnrolleeChangeEvent.class);

    public void enrolleeChange(EnrolleeChangeEvent event);
}
