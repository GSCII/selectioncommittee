package ru.ncedu.sc.gui.enrollee;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import ru.ncedu.sc.dao.DocumentTypeDAO;
import ru.ncedu.sc.dao.EnrolleeDAO;
import ru.ncedu.sc.gui.enrollee.statement.EnrolleeStatementWindow;
import ru.ncedu.sc.model.Document;
import ru.ncedu.sc.model.DocumentType;
import ru.ncedu.sc.model.Enrollee;
import ru.ncedu.sc.utils.DaoFactory;

import java.util.List;

@Deprecated
public class EnrolleeViewByTable extends HorizontalLayout implements View, ValueChangeListener {
    private AbstractOrderedLayout menu;
    private EnrolleeTable content;

    public EnrolleeViewByTable() {
        menu = new VerticalLayout();
        menu.setWidth(300f, Unit.PIXELS);
        menu.setMargin(true);
        menu.setSpacing(true);

        TextField searchField = new TextField();
        searchField.setWidth(100f, Unit.PERCENTAGE);
        searchField.setInputPrompt("ФИО");
        searchField.setNullRepresentation("");
        searchField.addValueChangeListener(this);
        menu.addComponent(searchField);

        Button addEnrolleeButton = new Button();
        addEnrolleeButton.setCaption("Добавить абитуриента");
        addEnrolleeButton.setWidth(100f, Unit.PERCENTAGE);
        addEnrolleeButton.addStyleName("primary");
        addEnrolleeButton.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent clickEvent) { // Возможно, стоит перенести обработку в this
                Enrollee enrollee = new Enrollee();
                Document passport = new Document();

                // Возможно, стоит DocumentType сделать enum и убрать связь с БД
                passport.setType(DaoFactory.getDocumentTypeDAO().read(1));

                enrollee.setPassport(passport);
                enrollee.setDiploma(new Document());
                Window window = new EnrolleeWindow(enrollee);
                window.addCloseListener(new Window.CloseListener() {
                    @Override
                    public void windowClose(Window.CloseEvent closeEvent) {
                        refresh();
                    }
                });
                getUI().addWindow(window);
            }
        });
        menu.addComponent(addEnrolleeButton);

        Button addStatementButton = new Button();
        addStatementButton.setCaption("Добавить заявление");
        addStatementButton.setWidth(100f, Unit.PERCENTAGE);
        addStatementButton.addStyleName("primary");
        addStatementButton.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent clickEvent) {
                EnrolleeStatementWindow window = null;
                EnrolleeDAO dao = DaoFactory.getEnrolleeDAO();
                Integer enrolleeId = (Integer) content.getValue();
                if (enrolleeId != null) {
                    window = new EnrolleeStatementWindow(dao.read(enrolleeId));
                    window.addCloseListener(new Window.CloseListener() {
                        @Override
                        public void windowClose(Window.CloseEvent closeEvent) {
                            refresh();
                        }
                    });
                    getUI().addWindow(window);

                } else {
                    Notification.show("Для начала, выберите абитуриента в списке", Notification.Type.TRAY_NOTIFICATION);
                }
            }
        });
        menu.addComponent(addStatementButton);

        Button coverPageButton = new Button();
        coverPageButton.setCaption("Титульный лист");
        coverPageButton.setWidth(100f, Unit.PERCENTAGE);
        coverPageButton.addStyleName("primary");
        menu.addComponent(coverPageButton);

        Button documentStatementButton = new Button();
        documentStatementButton.setCaption("Заявление");
        documentStatementButton.setWidth(100f, Unit.PERCENTAGE);
        documentStatementButton.addStyleName("primary");
        menu.addComponent(documentStatementButton);

        Button receiptButton = new Button();
        receiptButton.setCaption("Расписка");
        receiptButton.setWidth(100f, Unit.PERCENTAGE);
        receiptButton.addStyleName("primary");
        menu.addComponent(receiptButton);

        content = new EnrolleeTable();
        content.setSizeFull();

        addComponents(menu, content);
        setExpandRatio(content, 1); // растягиваем только таблицу
        setSizeFull();
    }

    public void refresh() {
        try {
            List<Enrollee> entities = null;
            entities = DaoFactory.getEnrolleeDAO().readBlock(0, 10);
            content.load(entities);
        } catch (Exception e) {
            Notification.show(
                    "Ошибка при инициализации таблицы",
                    e.toString(),
                    Notification.Type.ERROR_MESSAGE
            );
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        refresh();
    }

    @Override
    public void valueChange(ValueChangeEvent event) {
        String searchValue = null;
        try {
            searchValue = event.getProperty().getValue().toString();
            if (searchValue != null && !searchValue.isEmpty()) {
                EnrolleeDAO dao = DaoFactory.getEnrolleeDAO();
                content.load(dao.findByName(searchValue));
            } else {
                refresh();
            }
        } catch (Exception e) {
            Notification.show(
                    "Ошибка при попытке поиска",
                    e.toString(),
                    Notification.Type.ERROR_MESSAGE
            );
        }

    }
}
