package ru.ncedu.sc.gui.enrollee;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickListener;
import org.hibernate.validator.internal.engine.ValidatorImpl;
import ru.ncedu.sc.dao.EnrolleeDAO;
import ru.ncedu.sc.gui.ValidationFormLayout;
import ru.ncedu.sc.model.DocumentType;
import ru.ncedu.sc.model.Enrollee;
import ru.ncedu.sc.model.Language;
import ru.ncedu.sc.utils.DaoFactory;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Gorbatovskiy on 06.03.2016.
 */
public class EnrolleeWindow extends Window implements ClickListener {
    private BeanFieldGroup<Enrollee> binder;
    private Enrollee enrollee;
    private Button save;
    private Button cancel;

    private ValidationFormLayout general;
    private ValidationFormLayout passport;
    private ValidationFormLayout education;
    private ValidationFormLayout other;

    public EnrolleeWindow(Enrollee enrollee) {
        this.enrollee = enrollee;

        setCaption("Редактирование абитуриента");
        setModal(true);
        setClosable(true);
        setResizable(false);
        setDraggable(false);
        setWidth("600px");
        setHeight("600px");

        binder = new BeanFieldGroup<Enrollee>(Enrollee.class);
        binder.setItemDataSource(enrollee);
        binder.setBuffered(true);

        List<Language> languages = DaoFactory.getLanguageDAO().readAll();
        BeanItemContainer<Language> languagesContainer =
                new BeanItemContainer<>(Language.class, languages);

        List<DocumentType> types = DaoFactory.getDocumentTypeDAO().readAll();
        BeanItemContainer<DocumentType> typesContainer =
                new BeanItemContainer<>(DocumentType.class, types);


        general = new ValidationFormLayout();
        general.setValidationVisible(false);
        general.setSizeFull();
        general.setMargin(true);
        general.addComponent(binder.buildAndBind("Фамилия", "lastName", TextField.class));
        general.addComponent(binder.buildAndBind("Имя", "firstName", TextField.class));
        general.addComponent(binder.buildAndBind("Отчество", "middleName", TextField.class));
        general.addComponent(binder.buildAndBind("Дата рождения", "birthDate", DateField.class));

        passport = new ValidationFormLayout();
        passport.setValidationVisible(false);
        passport.setSizeFull();
        passport.setMargin(true);
        passport.addComponent(binder.buildAndBind("Номер", "passport.number", TextField.class));
        passport.addComponent(binder.buildAndBind("Гражданство", "citizenship", TextField.class));
        passport.addComponent(binder.buildAndBind("Дата выдачи", "passport.issuedOn", DateField.class));
        passport.addComponent(binder.buildAndBind("Кем выдан", "passport.authority", TextArea.class));

        education = new ValidationFormLayout();
        education.setValidationVisible(false);
        education.setSizeFull();
        education.setMargin(true);
        education.addComponent(binder.buildAndBind("Наименование ОУ", "schoolName", TextArea.class));
        education.addComponent(binder.buildAndBind("Год окончания", "graduationYear", TextField.class));
        education.addComponent(new ComboBox("Тип документа", typesContainer));
        education.addComponent(binder.buildAndBind("Номер", "diploma.number", TextField.class));
        education.addComponent(binder.buildAndBind("Средний балл", "mark", TextField.class));
        education.addComponent(new ComboBox("Иностранный язык", languagesContainer));
        education.addComponent(binder.buildAndBind("Оригинал", "diploma.authenticity", CheckBox.class));
        for (Iterator<Component> iterator = education.iterator(); iterator.hasNext(); ) {
            Component component = iterator.next();
            if (component instanceof ComboBox) {
                ComboBox box = (ComboBox) component;
                box.setNullSelectionAllowed(false);
                box.setNewItemsAllowed(false);
                if (box.getCaption().equals("Тип документа")) {
                    binder.bind(box, "diploma.type");
                } else {
                    binder.bind(box, "foreignLanguage");
                }
            }

        }

        other = new ValidationFormLayout();
        other.setValidationVisible(false);
        other.setSizeFull();
        other.setMargin(true);
        other.addComponent(binder.buildAndBind("Адрес прописки", "registration", TextField.class));
        other.addComponent(binder.buildAndBind("Адрес проживания", "domicile", TextField.class));
        other.addComponent(binder.buildAndBind("Домашний телефон", "homePhone", TextField.class));
        other.addComponent(binder.buildAndBind("Мобильный телефон", "mobileNumber", TextField.class));
        other.addComponent(binder.buildAndBind("ФИО родителя", "parentName", TextField.class));
        other.addComponent(binder.buildAndBind("Телефон родителя", "parentNumber", TextField.class));
        other.addComponent(binder.buildAndBind("Общежитие", "hostelProvision", CheckBox.class));
        other.addComponent(binder.buildAndBind("Льготы", "hasBenefits", CheckBox.class));
        other.addComponent(binder.buildAndBind("Материалы", "hasMaterials", CheckBox.class));


        TabSheet tabs = new TabSheet();
        tabs.setSizeFull();
        tabs.addStyleName("padded-tabbar");
        tabs.addTab(general, "Основная информация");
        tabs.addTab(passport, "Паспортные данные");
        tabs.addTab(education, "Образование");
        tabs.addTab(other, "Прочее");


        Label emptyLabel = new Label("");

        save = new Button("Сохранить");
        save.addStyleName("primary");
        save.addClickListener(this);

        cancel = new Button("Отмена");
        cancel.addClickListener(this);

        HorizontalLayout footer = new HorizontalLayout();
        footer.setWidth("100%");
        footer.setSpacing(true);
        footer.addStyleName("v-window-bottom-toolbar");
        footer.addComponent(emptyLabel);
        footer.setExpandRatio(emptyLabel, 1);
        footer.addComponent(save);
        footer.addComponent(cancel);


        VerticalLayout root = new VerticalLayout();
        root.setHeight("100%");
        root.addComponent(tabs);
        root.setExpandRatio(tabs, 1);
        root.addComponent(footer);
        setContent(root);
    }

    public void saveEnrollee() {
        general.setValidationVisible(true);
        passport.setValidationVisible(true);
        education.setValidationVisible(true);
        other.setValidationVisible(true);
        if (binder.isValid()) {
            EnrolleeDAO dao = null;
            try {
                binder.commit();
                dao = DaoFactory.getEnrolleeDAO();
                if (enrollee.getId() != null) {
                    dao.update(enrollee);
                    Notification.show("Абитуриент успешно изменен", Notification.Type.TRAY_NOTIFICATION);
                } else {
                    dao.create(enrollee);
                    Notification.show("Абитуриент успешно добавлен", Notification.Type.TRAY_NOTIFICATION);
                }
                close();
            } catch (Exception e) {
                Notification.show("Ошибка при взаимодействии с БД",
                        e.getMessage(), Notification.Type.ERROR_MESSAGE);
            }
        } else {
            Notification.show("Пожалуйста, заполните форму корректно", Notification.Type.TRAY_NOTIFICATION);
        }
    }

    @Override
    public void buttonClick(Button.ClickEvent clickEvent) {
        if (clickEvent.getButton() == save) {
            saveEnrollee();
        } else {
            close();
        }
    }
}
