package ru.ncedu.sc.gui.enrollee.statement;

import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickListener;
import ru.ncedu.sc.dao.StatementDAO;
import ru.ncedu.sc.model.Enrollee;
import ru.ncedu.sc.model.Speciality;
import ru.ncedu.sc.model.Statement;
import ru.ncedu.sc.utils.DaoFactory;
import ru.ncedu.sc.utils.ProgramConstants;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static com.vaadin.ui.Button.*;

/**
 * Created by Gorbatovskiy on 10.03.2016.
 */
public class EnrolleeStatementWindow extends Window implements ClickListener {
    private Enrollee enrollee;
    private List<Statement> statements;
    private List<Speciality> specialities;
    private List<EnrolleeStatementBlock> statementBlocks;
    private VerticalLayout statementLayout;

    private Button saveButton;
    private Button cancelButton;
    private Button addingButton;

    public EnrolleeStatementWindow(Enrollee enrollee) {
        this.enrollee = enrollee;

        statements = DaoFactory.getStatementDAO().findByEnrollee(enrollee.getId());
        if (statements == null) {
            statements = new LinkedList<>();
        }

        specialities = DaoFactory.getSpecialityDAO().readAll();
        if (specialities == null) {
            specialities = new LinkedList<>();
        }

        statementBlocks = new LinkedList<>();
        for (Statement statement : statements) {
            statementBlocks.add(new EnrolleeStatementBlock(statement, specialities));
        }

        setCaption("Cписок заявлений");
        setModal(true);
        setClosable(true);
        setResizable(false);
        setDraggable(false);
        setWidth("800px");

        Label enrolleeTitle = new Label(
                enrollee.getLastName() + " " + enrollee.getFirstName() + " " + enrollee.getMiddleName()
        );
        enrolleeTitle.setSizeFull();

        addingButton = new Button("Добавить новую запись");
        addingButton.addClickListener(this);

        HorizontalLayout header = new HorizontalLayout();
        header.setSizeFull();
        header.setSpacing(true);
        header.addComponent(enrolleeTitle);
        header.addComponent(addingButton);
        header.setExpandRatio(enrolleeTitle, 1);

        statementLayout = new VerticalLayout();
        statementLayout.setWidth("100%");
        statementLayout.setSpacing(true);
        statementLayout.setMargin(true);
        for (EnrolleeStatementBlock block : statementBlocks) {
            statementLayout.addComponent(block);
        }

        Panel statementPanel = new Panel();
        statementPanel.setWidth("100%");
        statementPanel.setHeight("500px");
        statementPanel.setContent(statementLayout);
        statementPanel.addStyleName("borderless");
        statementPanel.addStyleName("scroll-divider");

        Label spacing = new Label();
        spacing.setSizeFull();

        saveButton = new Button("Сохранить");
        saveButton.addStyleName("primary");
        saveButton.addClickListener(this);

        cancelButton = new Button("Отмена");
        cancelButton.addClickListener(this);

        HorizontalLayout footer = new HorizontalLayout();
        footer.setWidth("100%");
        footer.setSpacing(true);
        footer.addComponent(spacing);
        footer.addComponent(saveButton);
        footer.addComponent(cancelButton);
        footer.setExpandRatio(spacing, 1);

        AbstractOrderedLayout content = new VerticalLayout();
        content.setSpacing(true);
        content.setMargin(true);
        content.addComponent(header);
        content.addComponent(statementPanel);
        content.addComponent(footer);

        setContent(content);
    }

    public void addNewStatement() {
        if (statementBlocks.size() >= ProgramConstants.MAX_STATEMENT_COUNT) {
            return;
        }
        Statement statement = new Statement();
        statement.setEnrollee(enrollee);
        EnrolleeStatementBlock block = new EnrolleeStatementBlock(statement, specialities);
        statementBlocks.add(block);
        statementLayout.addComponent(block);
        center();
    }

    public void saveAllStatements() {
        StatementDAO dao = DaoFactory.getStatementDAO();
        Statement statement = null;
        // Реализовать проверку на приоритеты
        for (EnrolleeStatementBlock block : statementBlocks) {
            statement = block.getStatement();
            statement.setRelevance(true);
            statement.setChangesDate(new Date());
            // setUser (пока уберем NotNull зависимость)
            if (statement.getId() != null) {
                dao.update(statement);
            } else {
                dao.create(statement);
            }
        }
        close();
    }

    @Override
    public void buttonClick(ClickEvent clickEvent) {
        Button eventButton = clickEvent.getButton();
        if (eventButton == addingButton) {
//            Должно срабатывать только если
//            все предыдущие заполнены
            addNewStatement();
        } else if (eventButton == saveButton) {
            saveAllStatements();
        } else {
            close();
        }
    }
}
