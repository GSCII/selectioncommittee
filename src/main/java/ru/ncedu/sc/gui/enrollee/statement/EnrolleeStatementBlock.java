package ru.ncedu.sc.gui.enrollee.statement;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import ru.ncedu.sc.model.Speciality;
import ru.ncedu.sc.model.Statement;

import java.util.List;

/**
 * Created by Gorbatovskiy on 10.03.2016.
 */
public class EnrolleeStatementBlock extends CustomComponent implements ValueChangeListener {
    private BeanFieldGroup<Statement> binder;

    private OptionGroup tuitionGroup;   // Форма обучения
    private OptionGroup feesGroup;      // Форма оплаты

    public EnrolleeStatementBlock(Statement statement, List<Speciality> specialityList) {
        binder = new BeanFieldGroup<>(Statement.class);
        binder.setItemDataSource(statement);

        BeanItemContainer<Speciality> specialities
                = new BeanItemContainer<>(Speciality.class, specialityList);

        VerticalLayout content = new VerticalLayout();
        content.setWidth("100%");
        content.setSpacing(true);

        HorizontalLayout description = new HorizontalLayout();
        description.setSpacing(true);
        description.setWidth("100%");

        ComboBox priority = new ComboBox("Приоритет");
        priority.setNewItemsAllowed(false);
        priority.setNullSelectionAllowed(false);
        priority.setImmediate(true);
        for (int i = 1; i <= 5; i++) {
            priority.addItem(i);
        }
        binder.bind(priority, "priority");

        ComboBox speciality = new ComboBox("Специальность", specialities);
        speciality.setNewItemsAllowed(false);
        speciality.setNullSelectionAllowed(false);
        speciality.setImmediate(true);
        speciality.setWidth("100%");
        binder.bind(speciality, "speciality");
        speciality.addValueChangeListener(this);

        description.addComponent(priority);
        description.addComponent(speciality);
        description.setExpandRatio(speciality, 1);

        HorizontalLayout specification = new HorizontalLayout();
        specification.setSpacing(true);
        specification.setWidth("100%");

        tuitionGroup = new OptionGroup();
        tuitionGroup.addItems(Boolean.TRUE, Boolean.FALSE);
        tuitionGroup.setItemCaption(Boolean.TRUE, "Очно");
        tuitionGroup.setItemCaption(Boolean.FALSE, "Заочно");
        tuitionGroup.setItemEnabled(Boolean.TRUE, false);
        tuitionGroup.setItemEnabled(Boolean.FALSE, false);
        tuitionGroup.setNullSelectionAllowed(false);
        binder.bind(tuitionGroup, "fullTime");

        feesGroup = new OptionGroup();
        feesGroup.addItems(Boolean.TRUE, Boolean.FALSE);
        feesGroup.setItemCaption(Boolean.TRUE, "Бюджет");
        feesGroup.setItemCaption(Boolean.FALSE, "Внебюджет");
        feesGroup.setItemEnabled(Boolean.TRUE, false);
        feesGroup.setItemEnabled(Boolean.FALSE, false);
        feesGroup.setNullSelectionAllowed(false);
        binder.bind(feesGroup, "budget");

        specification.addComponent(tuitionGroup);
        specification.addComponent(feesGroup);
        specification.setExpandRatio(feesGroup, 1);

        content.addComponent(description);
        content.addComponent(specification);
        setCompositionRoot(content);
    }

    @Override
    public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
        Speciality speciality = (Speciality) valueChangeEvent.getProperty().getValue();
        if (speciality.isBudget()) {
            feesGroup.setItemEnabled(Boolean.TRUE, true);
        } else {
            feesGroup.setItemEnabled(Boolean.TRUE, false);
            feesGroup.unselect(Boolean.TRUE);
        }
        if (speciality.isCommerce()) {
            feesGroup.setItemEnabled(Boolean.FALSE, true);
        } else {
            feesGroup.setItemEnabled(Boolean.FALSE, false);
            feesGroup.unselect(Boolean.FALSE);
        }
        if (speciality.isFullTime()) {
            tuitionGroup.setItemEnabled(Boolean.TRUE, true);
        } else {
            tuitionGroup.setItemEnabled(Boolean.TRUE, false);
            tuitionGroup.unselect(Boolean.TRUE);
        }
        if (speciality.isPartTime()) {
            tuitionGroup.setItemEnabled(Boolean.FALSE, true);
        } else {
            tuitionGroup.setItemEnabled(Boolean.FALSE, false);
            tuitionGroup.unselect(Boolean.FALSE);
        }
    }

    public Statement getStatement() {
        Statement statement = null;
        try {
            binder.commit();
            statement = binder.getItemDataSource().getBean();
        } catch (FieldGroup.CommitException e) {
            Notification.show("Statement commit error", Notification.Type.ERROR_MESSAGE);
        }
        return statement;
    }
}
