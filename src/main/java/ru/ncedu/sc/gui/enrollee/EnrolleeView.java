package ru.ncedu.sc.gui.enrollee;


import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import ru.ncedu.sc.dao.EnrolleeDAO;
import ru.ncedu.sc.gui.enrollee.statement.EnrolleeStatementWindow;
import ru.ncedu.sc.model.Document;
import ru.ncedu.sc.model.Enrollee;
import ru.ncedu.sc.utils.DaoFactory;

import java.lang.reflect.Method;

/**
 * Created by Gorbatovskiy on 20.03.2016.
 */
public class EnrolleeView extends CustomComponent implements View, ValueChangeListener, ClickListener, EnrolleeChangeListener {
    private BeanItemContainer<Enrollee> container;
    private HorizontalLayout root;
    private VerticalLayout menu;
    private EnrolleeGrid content;

    private Button addEnrolleeButton;
    private Button addStatementButton;
    private Button coverPageButton;
    private Button documentStatementButton;
    private Button receiptButton;

    public EnrolleeView() {
        menu = new VerticalLayout();
        menu.setWidth(300f, Unit.PIXELS);
        menu.setMargin(true);
        menu.setSpacing(true);

        TextField searchField = new TextField();
        searchField.setWidth(100f, Unit.PERCENTAGE);
        searchField.setInputPrompt("ФИО");
        searchField.setNullRepresentation("");
        searchField.addValueChangeListener(this);
        menu.addComponent(searchField);

        addEnrolleeButton = new Button();
        addEnrolleeButton.setCaption("Добавить абитуриента");
        addEnrolleeButton.setWidth(100f, Unit.PERCENTAGE);
        addEnrolleeButton.addStyleName("primary");
        addEnrolleeButton.addClickListener(this);
        menu.addComponent(addEnrolleeButton);

        addStatementButton = new Button();
        addStatementButton.setCaption("Добавить заявление");
        addStatementButton.setWidth(100f, Unit.PERCENTAGE);
        addStatementButton.addStyleName("primary");
        addStatementButton.addClickListener(this);
        menu.addComponent(addStatementButton);

        coverPageButton = new Button();
        coverPageButton.setCaption("Титульный лист");
        coverPageButton.setWidth(100f, Unit.PERCENTAGE);
        coverPageButton.addStyleName("primary");
        menu.addComponent(coverPageButton);

        documentStatementButton = new Button();
        documentStatementButton.setCaption("Заявление");
        documentStatementButton.setWidth(100f, Unit.PERCENTAGE);
        documentStatementButton.addStyleName("primary");
        menu.addComponent(documentStatementButton);

        receiptButton = new Button();
        receiptButton.setCaption("Расписка");
        receiptButton.setWidth(100f, Unit.PERCENTAGE);
        receiptButton.addStyleName("primary");
        menu.addComponent(receiptButton);

        container = new BeanItemContainer<Enrollee>(Enrollee.class);
        container.addNestedContainerBean("passport");
        container.addNestedContainerBean("foreignLanguage");
        container.addNestedContainerBean("diploma");
        content = new EnrolleeGrid(container);
        content.addEnrolleeChangeListener(this);
        content.setSizeFull();

        root = new HorizontalLayout();
        root.setSizeFull();
        root.addComponents(menu, content);
        root.setExpandRatio(content, 1);
        setCompositionRoot(root);
        setSizeFull();
    }

    public void refresh() {
        container.removeAllItems();
        container.addAll(DaoFactory.getEnrolleeDAO().readAll());
    }

    @Override
    public void enter(ViewChangeEvent event) {
        refresh();
    }

    @Override
    public void valueChange(ValueChangeEvent event) {
        String searchValue = event.getProperty().getValue().toString();
        if (searchValue != null && !searchValue.isEmpty()) {
            container.removeAllItems();
            container.addAll(DaoFactory.getEnrolleeDAO().findByName(searchValue));
        } else {
            refresh();
        }
    }

    @Override
    public void buttonClick(ClickEvent event) {
        Button eventButton = event.getButton();
        if (eventButton == addEnrolleeButton) {
            enrolleeChange(new EnrolleeChangeEvent(this, null));
        }


//
//        EnrolleeStatementWindow window = null;
//        EnrolleeDAO dao = DaoFactory.getEnrolleeDAO();
//        Integer enrolleeId = (Integer) content.getValue();
//        if (enrolleeId != null) {
//            window = new EnrolleeStatementWindow(dao.read(enrolleeId));
//            window.addCloseListener(new Window.CloseListener() {
//                @Override
//                public void windowClose(Window.CloseEvent closeEvent) {
//                    refresh();
//                }
//            });
//            getUI().addWindow(window);
//
//        } else {
//            Notification.show("Для начала, выберите абитуриента в списке", Notification.Type.TRAY_NOTIFICATION);
//        }
    }

    @Override
    public void enrolleeChange(EnrolleeChangeEvent event) {
        Enrollee enrollee = event.getEnrollee();
        Document passport;
        Document diploma;

        if (enrollee == null) {
            enrollee = new Enrollee();
            passport = new Document();
            diploma = new Document();

            // Возможно, стоит DocumentType сделать enum и убрать связь с БД
            passport.setType(DaoFactory.getDocumentTypeDAO().read(1));

            enrollee.setPassport(passport);
            enrollee.setDiploma(diploma);
        }

        Window window = new EnrolleeWindow(enrollee);
        window.addCloseListener(new Window.CloseListener() {
            @Override
            public void windowClose(Window.CloseEvent e) {
                refresh();
            }
        });

        getUI().addWindow(window);
    }

}
