package ru.ncedu.sc.gui.enrollee;

import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import ru.ncedu.sc.model.Enrollee;

/**
 * Created by Gorbatovskiy on 20.03.2016.
 */
public class EnrolleeChangeEvent extends Component.Event {
    private Enrollee enrollee;

    public EnrolleeChangeEvent(Component source, Enrollee enrollee) {
        super(source);
        this.enrollee = enrollee;
    }

    public Enrollee getEnrollee() {
        return enrollee;
    }
}
