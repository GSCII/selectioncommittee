package ru.ncedu.sc.gui.enrollee;

import com.vaadin.data.util.converter.Converter;
import com.vaadin.server.FontAwesome;

import java.util.Locale;

/**
 * Created by Gorbatovskiy on 20.03.2016.
 */

/* DOES NOT WORK */
public class DefaultFontAwesomeConverter implements Converter<String, Integer> {
    private FontAwesome defaultValue;

    public DefaultFontAwesomeConverter(FontAwesome defaultValue) {
        this.defaultValue = defaultValue;
    }


    @Override
    public Integer convertToModel(String s, Class<? extends Integer> aClass, Locale locale) throws ConversionException {
        return null;
    }

    @Override
    public String convertToPresentation(Integer integer, Class<? extends String> aClass, Locale locale) throws ConversionException {
        return defaultValue.getHtml();
    }

    @Override
    public Class<Integer> getModelType() {
        return Integer.class;
    }

    @Override
    public Class<String> getPresentationType() {
        return String.class;
    }
}
