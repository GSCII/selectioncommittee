package ru.ncedu.sc.gui.enrollee;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.data.util.converter.StringToBooleanConverter;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;

import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.renderers.HtmlRenderer;
import ru.ncedu.sc.model.Enrollee;

import static com.vaadin.ui.renderers.ClickableRenderer.*;

/**
 * Created by Gorbatovskiy on 20.03.2016.
 */
public class EnrolleeGrid extends CustomComponent implements RendererClickListener {
    private Grid content;

    public EnrolleeGrid(BeanItemContainer<Enrollee> container) {
        // обертка для добавления нового столбца
        GeneratedPropertyContainer customContainer = new GeneratedPropertyContainer(container);
        customContainer.addGeneratedProperty("edit", new PropertyValueGenerator<FontAwesome>() {

            @Override
            public FontAwesome getValue(Item item, Object itemId, Object propertyId) {
                return FontAwesome.EDIT;
            }

            @Override
            public Class<FontAwesome> getType() {
                return FontAwesome.class;
            }
        });

        content = new Grid();
        content.setSizeFull();

        content.addColumn("lastName")
                .setHeaderCaption("Фамилия")
                .setExpandRatio(1);

        content.addColumn("firstName")
                .setHeaderCaption("Имя")
                .setExpandRatio(1);

        content.addColumn("middleName")
                .setHeaderCaption("Отчество")
                .setExpandRatio(1);

        content.addColumn("mark")
                .setHeaderCaption("Балл");

        content.addColumn("hasMaterials", Boolean.class)
                .setHeaderCaption("Мат.")
                .setResizable(false)
                .setRenderer(new HtmlRenderer(), new StringToBooleanConverter(
                        FontAwesome.CHECK_CIRCLE_O.getHtml(),
                        FontAwesome.CIRCLE_O.getHtml()));

        content.addColumn("diploma.authenticity", Boolean.class)
                .setHeaderCaption("Ориг.")
                .setResizable(false)
                .setRenderer(new HtmlRenderer(), new StringToBooleanConverter(
                        FontAwesome.CHECK_CIRCLE_O.getHtml(),
                        FontAwesome.CIRCLE_O.getHtml()));

        content.addColumn("edit")
                .setResizable(false)
                .setRenderer(new ButtonRenderer(this));

        content.setContainerDataSource(customContainer);
        setCompositionRoot(content);
    }

    public void addEnrolleeChangeListener(EnrolleeChangeListener listener) {
        addListener(EnrolleeChangeEvent.class, listener, EnrolleeChangeListener.ENROLLEE_CHANGE_METHOD);
    }

    @Override
    public void click(RendererClickEvent event) {
        fireEvent(new EnrolleeChangeEvent(this, (Enrollee) event.getItemId()));
    }
}
