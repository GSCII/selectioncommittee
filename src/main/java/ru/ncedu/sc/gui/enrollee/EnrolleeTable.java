package ru.ncedu.sc.gui.enrollee;


import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.navigator.Navigator;
import com.vaadin.ui.*;
import com.vaadin.ui.Table.Align;
import ru.ncedu.sc.dao.EnrolleeDAO;
import ru.ncedu.sc.model.Enrollee;
import ru.ncedu.sc.utils.DaoFactory;

import java.util.List;

/**
 * Created by Gorbatovskiy on 28.02.2016.
 */
@Deprecated
public class EnrolleeTable extends HorizontalLayout implements Handler {
    private Action edit;
    private Table content;

    public EnrolleeTable() {
        edit = new Action("Редактировать абитуриента");

        content = new Table();
        content.setImmediate(true);
        content.setSelectable(true);
        content.setSortEnabled(false);
        content.setNullSelectionAllowed(false);
        content.setPageLength(30);
        content.setEditable(false);
        content.setSizeFull();

        content.addContainerProperty(1, String.class, null,
                "Полное имя",
                null, Align.LEFT);
        content.addContainerProperty(2, Float.class, null,
                "Ср. балл",
                null, Align.LEFT);
        content.addContainerProperty(3, CheckBox.class, null,
                "Мат.",
                null, Align.LEFT);
        content.addContainerProperty(4, CheckBox.class, null,
                "Ориг.",
                null, Align.LEFT);

        content.setColumnExpandRatio(1, 5);
        content.setColumnExpandRatio(2, 1);
        content.setColumnExpandRatio(3, 1);
        content.setColumnExpandRatio(4, 1);

        content.addActionHandler(this);
        addComponent(content);
    }

    public void load(List<Enrollee> values) {
        content.removeAllItems();
        for (Enrollee enrollee : values) {
            if (enrollee.getId() != null) {
                CheckBox materials = new CheckBox();
                materials.setValue(enrollee.isHasMaterials());
                materials.setReadOnly(true);

                CheckBox authenticity = new CheckBox();
                authenticity.setValue(enrollee.getDiploma().isAuthenticity());
                authenticity.setReadOnly(true);

                StringBuffer fullName = new StringBuffer();
                fullName.append(enrollee.getLastName()).append(" ");
                fullName.append(enrollee.getFirstName()).append(" ");
                fullName.append(enrollee.getMiddleName());

                content.addItem(new Object[]{
                        fullName.toString(),
                        enrollee.getMark(),
                        materials,
                        authenticity
                }, enrollee.getId());
            } else {
                Notification.show(
                        "Ошибка отображения таблицы",
                        "Неверный идентификатор записи",
                        Notification.Type.ERROR_MESSAGE
                );
            }
        }
    }

    @Override
    public Action[] getActions(Object target, Object sender) {
        return new Action[]{edit};
    }

    @Override
    public void handleAction(Action action, Object sender, Object target) {
        if (action == edit) {
            Enrollee enrollee = DaoFactory.getEnrolleeDAO().read((Integer) target);
            EnrolleeWindow window = new EnrolleeWindow(enrollee);
            window.addCloseListener(new Window.CloseListener() {
                @Override
                public void windowClose(Window.CloseEvent closeEvent) {
                    HasComponents parent = getParent();
                    if (parent instanceof EnrolleeView) {
                        ((EnrolleeView) parent).refresh();
                    }
                }
            });
            getUI().addWindow(window);
        }
    }

    // returning selected item
    public Object getValue() {
        return content.getValue();
    }
}