package ru.ncedu.sc.utils;

import ru.ncedu.sc.dao.*;

/**
 * Created by Gorbatovskiy on 06.03.2016.
 */
public class DaoFactory {
    private static EnrolleeDAO enrolleeDAO;
    private static LanguageDAO languageDAO;
    private static SpecialityDAO specialityDAO;
    private static DocumentTypeDAO documentTypeDAO;
    private static StatementDAO statementDAO;
    private static UserDAO userDAO;

    public synchronized static EnrolleeDAO getEnrolleeDAO() {
        if (enrolleeDAO == null) {
            enrolleeDAO = new EnrolleeDAO();
        }
        return enrolleeDAO;
    }


    public synchronized static LanguageDAO getLanguageDAO() {
        if (languageDAO == null) {
            languageDAO = new LanguageDAO();
        }
        return languageDAO;
    }

    public synchronized static StatementDAO getStatementDAO() {
        if (statementDAO == null) {
            statementDAO = new StatementDAO();
        }
        return statementDAO;
    }

    public synchronized static SpecialityDAO getSpecialityDAO() {
        if (specialityDAO == null) {
            specialityDAO = new SpecialityDAO();
        }
        return specialityDAO;
    }

    public synchronized static DocumentTypeDAO getDocumentTypeDAO() {
        if (documentTypeDAO == null) {
            documentTypeDAO = new DocumentTypeDAO();
        }
        return documentTypeDAO;
    }

    public synchronized static UserDAO getUserDAO() {
        if (userDAO == null) {
            userDAO = new UserDAO();
        }
        return userDAO;
    }
}
