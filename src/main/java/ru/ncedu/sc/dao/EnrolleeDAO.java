package ru.ncedu.sc.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import ru.ncedu.sc.model.Document;
import ru.ncedu.sc.model.Enrollee;
import ru.ncedu.sc.model.Language;
import ru.ncedu.sc.utils.DaoFactory;
import ru.ncedu.sc.utils.HibernateUtil;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Gorbatovskiy on 18.02.2016.
 */
public class EnrolleeDAO extends GenericDAO<Enrollee, Integer> {
    public EnrolleeDAO() {
        super(Enrollee.class);
    }

    public List<Enrollee> findByName(String name) {
        List<Enrollee> entities = null;
        if (name != null && !name.equals("")) {
            name = name.trim().toLowerCase();
            StringBuilder query = new StringBuilder();
            query.append("SELECT * FROM SC_ENROLLEE ");
            query.append("WHERE INSTR('").append(name).append("',LOWER(FIRST_NAME)) <> 0 ");
            query.append("AND INSTR('").append(name).append("',LOWER(MIDDLE_NAME)) <> 0 ");
            query.append("AND INSTR('").append(name).append("',LOWER(LAST_NAME)) <> 0; ");
            entities = findByQuery(query.toString());
        }
        return entities;
    }

    public List<Enrollee> readBlock(int index, int count) {
        String limit = "LIMIT " + index + ", " + count;
        return findByQuery("SELECT * FROM SC_ENROLLEE " + limit);
    }

    public static void main(String[] args) {
        Document passport = new Document();
        passport.setNumber("1234");
        passport.setType(DaoFactory.getDocumentTypeDAO().read(1));
        passport.setAuthenticity(true);

        Document diploma = new Document();
        diploma.setNumber("4321");
        diploma.setType(DaoFactory.getDocumentTypeDAO().read(2));
        diploma.setAuthenticity(true);

        Enrollee testEnrollee = new Enrollee();
        testEnrollee.setFirstName("Владислав");
        testEnrollee.setMiddleName("Владимирович");
        testEnrollee.setLastName("Горбатовский");
        testEnrollee.setBirthDate(new Date());
        testEnrollee.setCitizenship("gff");
        testEnrollee.setRegistration("");
        testEnrollee.setDomicile("");
        testEnrollee.setHomePhone("");
        testEnrollee.setMobileNumber("");
        testEnrollee.setParentName("");
        testEnrollee.setHasBenefits(true);
        testEnrollee.setHasMaterials(true);
        testEnrollee.setHostelProvision(false);
        testEnrollee.setSchoolName("");
        testEnrollee.setGraduationYear(0);
        testEnrollee.setMark(3.4f);
        testEnrollee.setForeignLanguage(DaoFactory.getLanguageDAO().read(1));
        testEnrollee.setDiploma(diploma);
        testEnrollee.setPassport(passport);
        EnrolleeDAO dao = DaoFactory.getEnrolleeDAO();
        dao.create(testEnrollee);
        System.out.println(dao.readBlock(0,1).size());

    }
}

