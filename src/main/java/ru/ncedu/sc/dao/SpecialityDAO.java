package ru.ncedu.sc.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import ru.ncedu.sc.model.Speciality;
import ru.ncedu.sc.utils.DaoFactory;
import ru.ncedu.sc.utils.HibernateUtil;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Gorbatovskiy on 18.02.2016.
 */
public class SpecialityDAO extends GenericDAO<Speciality, Integer> {
    public SpecialityDAO() {
        super(Speciality.class);
    }

    public List<Speciality> findByName(String name) {
        List<Speciality> entities = null;
        if (name != null && !name.equals("")) {
            name = name.trim().toLowerCase();
            StringBuilder query = new StringBuilder();
            query.append("SELECT * FROM SC_SPECIALITY_REF")
                    .append("WHERE LOWER(NAME) = '").append(name).append("' ")
                    .append("OR LOWER(CODE) = '").append(name).append("';");
            entities = findByQuery(query.toString());
        }
        return entities;
    }

    public static void main(String[] args) {
        SpecialityDAO dao = DaoFactory.getSpecialityDAO();
        Speciality speciality = new Speciality();
        speciality.setName("Архитектура");
        speciality.setCode("07.02.01");
        speciality.setBudget(true);
        speciality.setCommerce(true);
        speciality.setFullTime(true);
        speciality.setPartTime(false);
        speciality.setId((Integer) dao.create(speciality));
        System.out.println(dao.findByName("Архитектура").size());
        System.out.println(dao.findByName("07.02.01 ").size());
        dao.delete(speciality);
        System.out.println(dao.readAll().size());
    }

}
