package ru.ncedu.sc.dao;

import ru.ncedu.sc.model.Language;
import ru.ncedu.sc.utils.DaoFactory;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

/**
 * Created by Gorbatovskiy on 18.02.2016.
 */
public class LanguageDAO extends GenericDAO<Language, Integer> {
    public LanguageDAO() {
        super(Language.class);
    }

    public static void main(String[] args) {
        LanguageDAO dao = DaoFactory.getLanguageDAO();
//        Language english = new Language();
//        english.setName("English2");
//        dao.create(english);
//        Language lang = dao.read(1);
//        lang.setName("German");
//        dao.load(lang);
        for (Language lang : dao.readAll()) {
            System.out.println(lang.getName());
        }

    }
}
