package ru.ncedu.sc.dao;

import org.hibernate.Session;
import ru.ncedu.sc.model.User;
import ru.ncedu.sc.utils.HibernateUtil;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Gorbatovskiy on 19.02.2016.
 */
public class UserDAO extends GenericDAO<User, String> {
    public UserDAO() {
        super(User.class);
    }

    public List<User> findByName(String name) {
        List<User> entities = null;
        if (name != null && !name.equals("")) {
            name = name.trim().toLowerCase();
            String query = "SELECT * FROM USERS WHERE FULLNAME = '" + name + "';";
            entities = findByQuery(query);
        }
        return entities;
    }

    public static void main(String[] args) {

    }
}
