package ru.ncedu.sc.dao;

import com.vaadin.ui.Notification;
import ru.ncedu.sc.model.Statement;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Gorbatovskiy on 10.03.2016.
 */
public class StatementDAO extends GenericDAO<Statement, Integer> {

    public StatementDAO() {
        super(Statement.class);
    }

    public List<Statement> findByEnrollee(Integer id) {
        List<Statement> entities = null;
        if (id != null) {
            StringBuilder query = new StringBuilder();
            query.append("SELECT * FROM SC_STATEMENT ");
            query.append("WHERE ENROLLEE_ID = ").append(id).append(";");
            entities = findByQuery(query.toString());
        }
        return entities;
    }
}
