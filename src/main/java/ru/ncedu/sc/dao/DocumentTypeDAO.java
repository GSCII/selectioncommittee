package ru.ncedu.sc.dao;

import ru.ncedu.sc.model.DocumentType;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Gorbatovskiy on 18.02.2016.
 */
public class DocumentTypeDAO extends GenericDAO<DocumentType, Integer> {
    public DocumentTypeDAO() {
        super(DocumentType.class);
    }
}
