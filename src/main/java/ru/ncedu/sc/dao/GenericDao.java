package ru.ncedu.sc.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import ru.ncedu.sc.utils.HibernateUtil;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Gorbatovskiy on 18.02.2016.
 */
public abstract class GenericDAO<T, PK extends Serializable> {
    private Class type;

    public GenericDAO(Class type) {
        this.type = type;
    }

    public Serializable create(T entity) {
        Session session = null;
        Serializable id = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            id = session.save(entity);
            session.getTransaction().commit();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return id;
    }

    public T read(PK id) {
        Session session = null;
        T entity = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            entity = (T) session.get(type, id);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return entity;
    }

    public void update(T entity) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public void delete(T entity) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(entity);
            session.getTransaction().commit();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public List<T> readAll() {
        List<T> entities = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            entities = (List<T>) session.createCriteria(type).list();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return entities;
    }

    public List<T> findByQuery(String query) {
        List<T> entities = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            entities = (List<T>) session.createSQLQuery(query)
                    .addEntity(type)
                    .list();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return entities;
    }
}
